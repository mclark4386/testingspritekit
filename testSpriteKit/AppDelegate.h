//
//  AppDelegate.h
//  testSpriteKit
//
//  Created by Matthew Clark on 11/19/14.
//  Copyright (c) 2014 MotionMobs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

