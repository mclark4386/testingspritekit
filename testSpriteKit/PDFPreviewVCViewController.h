//
//  PDFPreviewVCViewController.h
//  testSpriteKit
//
//  Created by Matthew Clark on 11/19/14.
//  Copyright (c) 2014 MotionMobs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDFPreviewVCViewController : UIViewController
@property (strong,nonatomic) NSURL* pdfUrl;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
- (IBAction)back:(id)sender;

@end
