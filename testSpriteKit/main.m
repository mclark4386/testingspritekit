//
//  main.m
//  testSpriteKit
//
//  Created by Matthew Clark on 11/19/14.
//  Copyright (c) 2014 MotionMobs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
