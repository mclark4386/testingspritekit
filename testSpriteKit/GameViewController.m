//
//  GameViewController.m
//  testSpriteKit
//
//  Created by Matthew Clark on 11/19/14.
//  Copyright (c) 2014 MotionMobs. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"
#import "PDFPreviewVCViewController.h"

@implementation SKScene (Unarchive)

+ (instancetype)unarchiveFromFile:(NSString *)file {
    /* Retrieve scene file path from the application bundle */
    NSString *nodePath = [[NSBundle mainBundle] pathForResource:file ofType:@"sks"];
    /* Unarchive the file to an SKScene object */
    NSData *data = [NSData dataWithContentsOfFile:nodePath
                                          options:NSDataReadingMappedIfSafe
                                            error:nil];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

+ (instancetype)unarchiveFromKey:(NSString *)key {
    /* Unarchive the file to an SKScene object */
    NSData* data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    NSKeyedUnarchiver *arch = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    [arch setClass:self forClassName:@"SKScene"];
    SKScene *scene = [arch decodeObjectForKey:NSKeyedArchiveRootObjectKey];
    [arch finishDecoding];
    
    return scene;
}

- (void)archiveToKey:(NSString *)key {
    /* Unarchive the file to an SKScene object */
    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:self] forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end

@interface GameViewController ()
@property (strong,nonatomic) GameScene* scene;

@end

@implementation GameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    skView.ignoresSiblingOrder = YES;
    
    // Create and configure the scene.
    self.scene = [GameScene unarchiveFromFile:@"GameScene"];
    self.scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:self.scene];
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRoundedRect(path, &CGAffineTransformIdentity, CGRectMake(0, 0, 150, 150), 5, 5);
    SKShapeNode* sn = [SKShapeNode shapeNodeWithPath:path centered:YES];
    [sn setStrokeColor:[UIColor redColor] ];
    [sn setPosition:CGPointMake(50, 50)];
    [self.scene addChild:sn];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"pdf"]) {
        PDFPreviewVCViewController* vc = [segue destinationViewController];
        //TODO: render View to PDF, save to URL and then send URL
    }
}

-(void)renderViewToPdf:(NSString*)filename{
    UIGraphicsBeginPDFContextToFile( filename, self.view.bounds, nil );
    UIGraphicsBeginPDFPage();
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    [self.view.layer renderInContext:pdfContext];
    UIGraphicsEndPDFContext();
}

- (IBAction)save:(id)sender {
    [self.scene archiveToKey:@"test"];
}
- (IBAction)load:(id)sender {
    self.scene = [GameScene unarchiveFromKey:@"test"];
    self.scene.scaleMode = SKSceneScaleModeAspectFill;
    [((SKView*)self.view) presentScene:self.scene];
}
- (IBAction)clear:(id)sender {
    self.scene = [GameScene sceneWithSize:self.view.bounds.size];
    self.scene.scaleMode = SKSceneScaleModeAspectFill;
    [((SKView*)self.view) presentScene:self.scene];
    
}

@end
